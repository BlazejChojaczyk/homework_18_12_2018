#!/usr/bin/env python3

class Rectangle:
    def __init__(self, side1_length, side2_length, side1, side2):
        self.side1_length = side1_length
        self.side2_length = side2_length
        self.side1 = side1
        self.side2 = side2

    def __eq__(self, other):
        print("Comparision type: {} == {} and {} == {}".format(self.side1, other.side1, self.side2, other.side2))
        return self.side1_length == other.side1_length and self.side2_length == other.side2_length

    def __lt__(self, other):
        print("Comparision type: {} < {} and {} < {}".format(self.side1, other.side1, self.side2, other.side2))
        return self.side1_length < other.side1_length and self.side2_length < other.side2_length

    def __gt__(self, other):
        print("Comparision type: {} > {} and {} > {}".format(self.side1, other.side1, self.side2, other.side2))
        return self.side1_length > other.side1_length and self.side2_length > other.side2_length

    def __le__(self, other):
        print("Comparision type: {} <= {} and {} <= {}".format(self.side1, other.side1, self.side2, other.side2))
        return self.side1_length <= other.side1_length and self.side2_length <= other.side2_length

    def __ge__(self, other):
        print("Comparision type: {} >= {} and {} >= {}".format(self.side1, other.side1, self.side2, other.side2))
        return self.side1_length >= other.side1_length and self.side2_length >= other.side2_length

    def __ne__(self, other):
        print("Comparision type: {} != {} and {} != {}".format(self.side1, other.side1, self.side2, other.side2))
        return self.side1_length != other.side1_length and self.side2_length != other.side2_length


a = Rectangle(4, 2, "A1", "A2")
b = Rectangle(5, 3, "B1", "B2")

print("1. A == B = {}".format(a == b))
print("2. A < B = {}".format(a < b))
print("3. A > B = {}".format(a > b))
print("4. A <= B = {}".format(a <= b))
print("5. A >= B = {}".format(a >= b))
print("6. A != B = {}".format(a != b))