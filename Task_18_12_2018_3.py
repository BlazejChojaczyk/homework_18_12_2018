#!/usr/bin/env python3

class Cuboid:
    def __init__(self, side1_length, side2_length, height_length, side1, side2, height):
        self.side1_length = side1_length
        self.side2_length = side2_length
        self.height_length = height_length
        self.side1 = side1
        self.side2 = side2
        self.height = height

    def __eq__(self, other):
        print("Comparision type: {} == {} and {} == {} and {} == {}".format(self.side1, other.side1, self.side2, other.side2, self.height, other.height))
        return self.side1_length == other.side1_length and self.side2_length == other.side2_length and self.height_length == other.height_length

    def __lt__(self, other):
        print("Comparision type: {} < {} and {} < {} and {} < {}".format(self.side1, other.side1, self.side2, other.side2, self.height, other.height))
        return self.side1_length < other.side1_length and self.side2_length < other.side2_length and self.height_length < other.height_length

    def __gt__(self, other):
        print("Comparision type: {} > {} and {} > {} and {} > {}".format(self.side1, other.side1, self.side2, other.side2, self.height, other.height))
        return self.side1_length > other.side1_length and self.side2_length > other.side2_length and self.height_length > other.height_length

    def __le__(self, other):
        print("Comparision type: {} <= {} and {} <= {} and {} <= {}".format(self.side1, other.side1, self.side2, other.side2, self.height, other.height))
        return self.side1_length <= other.side1_length and self.side2_length <= other.side2_length and self.height_length <= other.height_length

    def __ge__(self, other):
        print("Comparision type: {} >= {} and {} >= {} and {} >= {}".format(self.side1, other.side1, self.side2, other.side2, self.height, other.height))
        return self.side1_length >= other.side1_length and self.side2_length >= other.side2_length and self.height_length >= other.height_length

    def __ne__(self, other):
        print("Comparision type: {} != {} and {} != {} and {} != {}".format(self.side1, other.side1, self.side2, other.side2, self.height, other.height))
        return self.side1_length != other.side1_length and self.side2_length != other.side2_length and self.height_length != other.height_length


a = Cuboid(4, 2, 5, "A1", "A2", "AH")
b = Cuboid(5, 3, 6, "B1", "B2", "BH")

print("1. A == B = {}".format(a == b))
print("2. A < B = {}".format(a < b))
print("3. A > B = {}".format(a > b))
print("4. A <= B = {}".format(a <= b))
print("5. A >= B = {}".format(a >= b))
print("6. A != B = {}".format(a != b))